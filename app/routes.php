<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

//user API
Route::group(['prefix' => 'user','before'=>''], function () {

//login
	Route::post('login/auth','AuthController@LoginUser');
	Route::get('login/destroy','AuthController@LogoutUser');

//report
	Route::get('report/','LaporanController@getAllReport');
	Route::get('report_detail/{id}','LaporanController@getReport');
	
	Route::post('create_report/','LaporanController@createReport');
	Route::put('edit_report/{id}','LaporanController@editReport');
	Route::delete('delete_report/{id}','LaporanController@deleteReport');

//solusi
	Route::get('solusi_detail/{id}','LaporanController@getSolusi');
	
// comment
	Route::get('comment_report/{id}','CommentController@getCommentReport');
	Route::get('comment_proposal/{id}','CommentController@getCommentProposal');
	Route::get('comment_anggaran/{id}','CommentController@getCommentAnggaran');
	
	Route::post('create_comment_report/{report}','CommentController@createCommentReport');
	Route::post('create_comment_proposal/{proposal}','CommentController@createCommentProposal');
	Route::post('create_comment_Anggaran/{anggaran}','CommentController@createCommentAnggaran');

	Route::delete('delete_comment/{id}','CommentController@deleteComment');
	
//get school
	Route::get('school/{jenjang}/{akreditasi}/{nama_sekolah}','SchoolController@getAllSchool');
	Route::get('school_detail/{id}','SchoolController@getSchool');
	
	Route::get('school_report/{id}','SchoolController@getSchoolReport');
	Route::get('school_proposal/{id}','SchoolController@getSchoolProposal');
	Route::get('school_anggaran/{id}','SchoolController@getSchoolAnggaran');
	
//get proposal
	Route::get('proposal/','ProposalController@getAllProposal');
	Route::get('proposal_detail/{id}','ProposalController@getProposal');

//get anggaran
	Route::get('anggaran/','AnggaranController@getAllAnggaran');
	Route::get('anggaran_detail/{id}','AnggaranController@getAnggaran');
//
});

//school API
Route::group(['prefix' => 'school','before'=>''], function () {

//login
	Route::post('login/auth','AuthController@LoginSchoool');
	Route::get('login/destroy','AuthController@Logout');
	
//report
	Route::get('report/','LaporanController@getAllReportSchool');
	Route::get('report_detail/{id}','LaporanController@getReport');
	
	Route::put('report_update/{id}','LaporanController@editReport');
	Route::delete('delete_report/{id}','LaporanController@deleteReport');
	
//proposal
	Route::get('proposal/','ProposalController@getSchoolProposal');
	Route::get('proposal_detail/{id}','ProposalController@getProposal');
	Route::post('create_proposal/','ProposalController@createProposal');
	Route::put('update_proposal/{id}','ProposalController@editProposal');
	Route::delete('delete_proposal/{id}','ProposalController@deleteProposal');

//anggaran	
	Route::get('anggaran/','AnggaranController@getSchoolAnggaran');
	Route::get('anggaran_detail/{id}','AnggaranController@getAnggaran');
	
//solusi
	Route::post('create_solusi/','LaporanController@createSolusi');
	Route::put('update_solusi/{id}','LaporanController@editSolusi');
	Route::delete('delete_solusi/{id}','LaporanController@deleteSolusi');
	
//comment
	Route::get('comment_report/{id}','CommentController@getCommentReport');
	Route::get('comment_proposal/{id}','CommentController@getCommentProposal');
	Route::get('comment_anggaran/{id}','CommentController@getCommentAnggaran');
	
	Route::post('create_comment_report/{report}','CommentController@createCommentReport');
	Route::post('create_comment_proposal/{proposal}','CommentController@createCommentProposal');
	Route::post('create_comment_Anggaran/{anggaran}','CommentController@createCommentAnggaran');

	Route::delete('delete_comment/{id}','CommentController@deleteComment');
});

//gov API
Route::group(['prefix' => 'gov','before'=>''], function () {

	Route::post('login/auth','AuthController@LoginGov');
	Route::get('login/destroy','AuthController@Logout');
	
//school
	Route::get('school/{jenjang}/{akreditasi}/{nama_sekolah}','SchoolController@getAllSchool');
	Route::get('school_detail/{id}','SchoolController@getSchool');
	
	Route::get('school_report/{id}','SchoolController@getSchoolReport');
	Route::get('school_proposal/{id}','SchoolController@getSchoolProposal');
	Route::get('school_anggaran/{id}','SchoolController@getSchoolAnggaran');
	

	Route::get('report_detail/{id}','LaporanController@getReport');
	Route::get('proposal_detail/{id}','ProposalController@getProposal');
	Route::get('anggaran_detail/{id}','AnggaranController@getAnggaran');
	
	
	Route::post('create_anggaran/','AnggaranController@createAnggaran');
	Route::put('update_anggaran/{id}','AnggaranController@editAnggaran');
	Route::delete('delete_anggaran/{id}','AnggaranController@deleteAnggaran');
	
});
