<?php
class CommentController extends Controller {
	
	
	//dapetin comment report id tsb 
	public function getCommentReport($id){
		DB::table('comment')
		->where('ID_Report',$id)
		->get();
	}
	//dapetin comment proposal id tsb
	public function getCommentProposal($id){
		
	}
	
	//dapetin comment anggaran id tsb
	public function getCommentAnggaran($id){
		
	}
	//buat comment report
	public function createCommentReport($report){
		
		$res = DB::table('comment')->insert(
		array('ID_Report' => $report, 'Commentator'=> Session::get('id_user')
		'Isi'=> Input::get('isi'), 'isAnonymous'=> Input::get('anonymous'),
		'Date'=> date('Y-m-d H:i:s')));
	
		if($res){
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
		
	}
	
	//buat comment proposal
	public function createCommentProposal($proposal){
		
	}
	
	//buat comment anggaran
	public function createCommentAnggaran($anggaran){
		
	}
	
	//delete comment
	public function deleteComment($id){
		$res= DB::table('comment')
		->where('ID_Comment',$id)
		->delete();
		
		if($res){
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
	}
	
	
	
	
}

?>