<?php
class ProposalController extends Controller {
	
	
	//kluarin semua proposal yang ada
	public function getAllProposal(){
		
		$proposal = DB::table('proposal')
		->join('sekolah','sekolah.ID_Sekolah','=','proposal.ID_Sekolah')
		->get();
		
		//bisa dtmbhin where apa
		return Response::json($proposal);
	}
	
	//dapetin proposal id tsb 
	public function getProposal($id){
		$proposal = DB::table('proposal')
		->join('sekolah','proposal.ID_Sekolah','=','sekolah.ID_Sekolah')
		->where('proposal.ID_Proposal',$id)
		->get();
		
		return Response::json($proposal);
	}
	//dapetin proposal id sekolah tsb
	public function getSchoolProposal($id){
		
		$proposal = DB::table('proposal')
		->join('sekolah','proposal.ID_Sekolah','=','sekolah.ID_Sekolah')
		->where('proposal.ID_Sekolah',$id)
		->get();
		
		return Response::json($proposal);
	}	
	
	
	//buat proposal
	public function createProposal(){
		$res = DB::table('proposal')->insert(
		array( 'ID_Sekolah' => Session::get('id_sekolah'), 
		'Judul'=> Input::get('judul'),'Abstrak'=> Input::get('abstrak'), 
		'Proposal_Url'=> Input::get('url'), 'Date'=> date('Y-m-d H:i:s')));
	
		if($res){
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
	}
	
	//edit proposal id tsb
	public function editProposal($id){
		
		DB::table('proposal')
				->where('ID_Proposal', $id)
				->update(array('Judul' => Input::get('judul'),'Abstrak'=> Input::get('abstrak'), 'Proposal_Url' => Input::get('url')));
	}
	
	//delete proposal id tsb
	public function deleteProposal($id){
		
		$res = DB::table('proposal')->where('ID_Proposal', '=', $id)->delete();
		
		if($res){
			return Response::json(array('status'=>'true'));
		}else{	
			return Response::json(array('status'=>'false'));
		}
	}
	
	

}

?>