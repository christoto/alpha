<?php
class SchoolController extends Controller {
	
	//keluarin semua skola plus filter
	public function getAllSchool($jenjang,$akreditasi,$nama_sekolah){
		
		$school = DB::table('sekolah');
		
		if($jenjang!='ALL'){
			$school = $school->where('Jenjang',$jenjang);
		}
		
		if($akreditasi !='ALL'){
		$school = $school->where('Akreditas',$akreditasi);
		}
		
		if($nama_sekolah != 'ALL'){
		$school = $school->where('Nama','LIKE', '%'.$nama_sekolah.'%');
		}
		
		$school = $school->get();
		
		
		
		return Response::json($school);
	}
	
	//kluarin detail sekolah
	public function getSchool($id){
		
			$school = DB::table('sekolah')->where('ID_Sekolah',$id)->first();
		return Response::json($school);
	}
	
	//kluarin report id skolah
	public function getSchoolReport($id){
		$school = DB::table('report')
		->join('normal_user','normal_user.ID_User','=','report.ID_User')
		->where('report.ID_Sekolah',$id)->get();
		return Response::json($school);
	}
	
	//kluarin proposal id skloah
	public function getSchoolProposal($id){
		$school = DB::table('proposal')
		->where('proposal.ID_Sekolah',$id)->get();
		return Response::json($school);
	}
	
	//kluarin anggaran id skolah
	public function getSchoolAnggaran($id){
		$school = DB::table('anggaran')
		->where('anggaran.ID_Sekolah',$id)->get();
		return Response::json($school);
	}
	
	

}

?>