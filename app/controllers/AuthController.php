<?php
class AuthController extends Controller {
	
	
	public function LoginUser(){
		$username = Input::get('username');
		$password = Input::get('password');
		
		if (Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			$uname = Auth::user()->username;
			Session::set('uname', 'user');
			Session::set('id_user', Auth::user()->id_user);
			
			return Response::json(array('status' => 'true', 'uname' => Session::get('uname')));
		}
		else
		{
			return Response::json(array('status' => 'false'));
		}
		
	}
	
	public function LogoutUser(){
		
		Auth::logout();
		Session::flush();
		return Response::json(array('status' => 'true'));
	}
	
	public function LoginSchool(){
		$username = Input::get('username');
		$password = Input::get('password');
		
		if (Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			$uname = Auth::user()->username;
			Session::set('uname', $uname);
			Session::set('id_user', Auth::user()->id_user);
			return View::make('pages.school');
		}
		else
		{
			return false;
		}
		
	}

	public function LoginGov(){
		$username = Input::get('username');
		$password = Input::get('password');
		
		if (Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			$uname = Auth::user()->username;
			Session::set('uname', $uname);
			Session::set('id_user', Auth::user()->id_user);
			return View::make('pages.gov');
		}
		else
		{
			return false;
		}
	}

	public function test($id){
	
	}
}

?>