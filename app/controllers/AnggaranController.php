<?php
class AnggaranController extends Controller {
	
	
	//kluarin semua anggaran yang ada
	public function getAllAnggaran(){
		
		$anggaran = DB::table('anggaran')
		->get();
		
		//bisa dtmbhin where apa
		return Response::json($anggaran);
	}
	
	//dapetin anggaran id tsb 
	public function getAnggaran($id){
		
		
		$anggaran = DB::table('anggaran')
		->where('ID_Anggaran',$id)
		->get();
		
		//bisa dtmbhin where apa
		return Response::json($anggaran);
	}
	
	//dapetin anggaran id sekolah tsb
	public function getSchoolAnggaran(){
		
		$id=-2;
		$anggaran = DB::table('anggaran')
		->leftJoin('proposal','anggaran.ID_Anggaran','=','proposal.ID_Anggaran')
		->where('proposal.ID_Sekolah',$id)
		->get();
		
		
		//bisa dtmbhin where apa
		return Response::json($anggaran);
	}	
	
	
	//buat anggaran
	public function createAnggaran(){
		
		$id = DB::table('anggaran')->insertGetId(
		array('Tahun' => Input::get('tahun'), 'Judul' => Input::get('judul'), 
		'Diknas_Pengaju'=> Session::get('diknas'), 'Abstrak'=> Input::get('abstrak'), 'Nominal' => Input::get('nominal'),
		'Anggaran_Url'=> Input::get('url'),'Tanggal_Pengajuan'=> Input::get('pengajuan')));
	
		
		if($id){
		
			DB::table('proposal')
				->where('ID_Proposal', $id)
				->update(array('Diknas_Penyetuju' => Session::get('diknas'),'ID_Anggaran'=> $id));
				
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
		
		//
	}
	
	//edit anggaran id tsb
	public function editAnggaran($id){
		
		DB::table('anggaran')
				->where('ID_Anggaran', $id)
				->update(array('Judul' => Input::get('judul'),'Abstrak'=> Input::get('abstrak'),
				'Tahun'=>Input::get('tahun'),'Anggaran_Url' => Input::get('url'),
				'Nominal'=> Input::get('nominal')));
	}
	
	//delete anggaran id tsb
	public function deleteAnggaran($id){
		
		$proposal = DB::table('proposal')
		->where('ID_Anggaran',$id)
		->update(array('ID_Anggaran'=>NULL,'Diknas_Penyetuju'=>NULL));
		//create null first
		
		$res = DB::table('anggaran')->where('ID_Anggaran', '=', $id)->delete();
		
		if($res){
			return Response::json(array('status'=>'true'));
		}else{	
			return Response::json(array('status'=>'false'));
		}
		
	}
	
	

}

?>