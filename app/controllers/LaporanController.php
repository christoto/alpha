<?php
class LaporanController extends Controller {
	
	
	//kluarin semua report yang ada
	public function getAllReport(){
		
		
		$report = DB::table('report')
		->join('normal_user','normal_user.ID_User','=','report.ID_User')
		->join('sekolah','sekolah.ID_Sekolah','=','report.ID_Sekolah')
		->get();
		
		//bisa dtmbhin where apa
		return Response::json($report);
	}
	
	//dapetin report id tsb 
	public function getReport($id){
		$report = DB::table('report')
		->join('normal_user','normal_user.ID_User','=','report.ID_User')
		->join('sekolah','proposal.ID_Sekolah','=','sekolah.ID_Sekolah')
		->where('report.ID_Report',$id)
		->get();
		
		return Response::json($report);
	}
	
	//buat report
	public function createReport(){
		
		$res = DB::table('report')->insert(
		array('ID_User' => Session::get('id_user'), 'ID_Sekolah' => Input::get('id_sekolah'), 
		'Isi'=> Input::get('isi'), 'Foto_Url'=> Input::get('foto'), 'isAnonymous' => Input::get('anonymous'), 'Status'=> 0, 'Date'=> date('Y-m-d H:i:s')));
	
		if($res){
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
	}
	
	//edit report id tsb
	public function editReport($id){
		
		if(isset(Input::get('stat'))){
			DB::table('report')
            ->where('ID_Report', $id)
            ->update(array('Status' => Input::get('status')));
		}else{
			DB::table('report')
				->where('ID_Report', $id)
				->update(array('Isi' => Input::get('isi'), 'Foto_Url' => Input::get('foto')));
		}
		
	}
	
	//delete report id tsb
	public function deleteReport($id){
	
		//delete comment nya juga
		$DB::table('comment')->where('ID_Report',$id)->delete();
		//delete
		$res = DB::table('report')->where('ID_Report', '=', $id)->delete();
		
		if($res){
			return Response::json(array('status'=>'true'));
		}else{	
			return Response::json(array('status'=>'false'));
		}
	}
	
	//get solusi dari id tsb(report/solusi)
	public function getSolusi($id){
		
		$report = DB::table('solusi')
		->leftJoin('anggaran','anggaran.ID_Anggaran','=','solusi.ID_Anggaran')
		->where('solusi.ID_Report',$id)
		->get();
		
		return Response::json($report);
	}
	
	//buat solusi
	public function createSolusi(){
		$res = DB::table('solusi')->insert(
		array('ID_Report' => Input::get('id_report'), 
		'Isi'=> Input::get('isi'), 'Foto_Url'=> Input::get('foto'),
		'ID_Anggaran' => NULL,
		'Date'=> date('Y-m-d H:i:s')));
	
		if($res){
			return Response::json(array('status'=>'true'));
		}else{
			return Response::json(array('status'=>'false'));
		}
	}
	
	//edit solusi
	public function editSolusi($id){
		
		DB::table('solusi')
            ->where('ID_Report', $id)
            ->update(array('Isi' => Input::get('isi'), 'Foto_Url' => Input::get('foto')));
	}
	
	//delete solusi
	public function deleteSolusi($id){
		
		$res = DB::table('solusi')->where('ID_Report', '=', $id)->delete();
		
		if($res){
			return Response::json(array('status'=>'false'));
		}else{	
			return Response::json(array('status'=>'false'));
		}
	}
	
	//kluarin smua report dari skolah tsb
	public function getAllReportSchool(){
	
		$report = DB::table('report')
		->join('normal_user','normal_user.ID_User','=','report.ID_User')
		->join('sekolah','sekolah.ID_Sekolah','=','sekolah.ID_Sekolah')
		->where('report.ID_Sekolah',Session::get('user_sekolah'))
		->get();
		
		//bisa dtmbhin where apa
		return Response::json($report);
		
		//session
	}
}

?>